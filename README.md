# steviaplus

A next little thing from STEVIA (Spatio-Temporal Visualization of Internet Attacks).

Requirements:
  * Elasticsearch 7.x (contains alerts from Wazuh)
  * Wazuh integration enabled [\[read more\]](https://wazuh.com/blog/how-to-integrate-external-software-using-integrator/)
  * Node.js LTS 14.x

To start this app:
  * Install dependencies

    `npm install`

  * Run the server

    `npm start`
