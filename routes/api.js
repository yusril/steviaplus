const fetch = require('node-fetch');
const express = require('express');
const app = require('../app');
const router = express.Router();

const { Client } = require('@elastic/elasticsearch');
const client = new Client({
  node: process.env.ELASTIC_URL,
  headers: { 'Authorization': 'Basic ' + process.env.ELASTIC_AUTH }
});

router.get('/', function (req, res, next) {
  res.status(418).end();
});

router.post('/alert', async function (req, res, next) {
  let alert = req.body;
  if (alert.src_ip) {
    // get country from ip
    await fetch('https://get.geojs.io/v1/ip/country/' + alert.src_ip)
      .then(response => response.text())
      .then(data => { alert.src_country = data.trim(); });
  }

  req.app.locals.socket.emitEvent('alert', alert);
  res.status(200).end();
});

router.get('/aggregate/daily', function (req, res, next) {
  const script = `
    String country;
    def coordinate;
    String sourceIp;
    String desc;
    String agentName;
    if(!doc['GeoLocation.country_iso_code'].empty){
      country = doc['GeoLocation.country_iso_code'].value;
    }
    if(!doc['data.srcip'].empty){
      sourceIp = doc['data.srcip'].value;
    }
    if(!doc['rule.description'].empty){
      desc = doc['rule.description'].value;
    }
    if(!doc['agent.name'].empty){
      agentName = doc['agent.name'].value;
    }
    String alert = country + '|' + coordinate + '|' + sourceIp + '|' + desc + '|' + agentName;
    return alert;
  `;
  client.search({
    index: 'wazuh-alerts*',
    body: {
      "query": {
        "constant_score": {
          "filter": {
            "range": {
              "timestamp": {
                "gte": "now-1d",
                "lte": "now"
              }
            }
          }
        }
      },
      "size": 0,
      "aggs": {
        "attacks": {
          "date_histogram": {
            "field": "timestamp",
            "interval": "14m"
          },
          "aggs": {
            "alerts": {
              "terms": {
                "script": {
                  "lang": "painless",
                  "source": script
                }
              }
            }
          }
        }
      }
    }
  }, (err, { body }) => {
    if (err) {
      console.log(err);
      res.status(404).end();
    }
    else res.json(body)
  })
});

module.exports = router;
