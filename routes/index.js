var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Stevia Plus' });
});

router.get('/history-old', function (req, res, next) {
  res.render('map/history-old');
});

router.get('/history', function (req, res, next) {
  res.render('map/history-new');
});

router.get('/realtime', function (req, res, next) {
  res.render('map/map');
});

router.get('/alert', function (req, res, next) {
  res.render('alert');
});

module.exports = router;
