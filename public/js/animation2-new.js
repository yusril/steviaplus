var useDummy = false;

var currentWidth = $('#map').width();
var width = 938;
var height = 620;

// var projection = d3.geoMercator()
//                   .scale(150)
//                   .translate([width / 2, height / 1.41]);

d3.selection.prototype.finish = function () {
  // check if there is a transition to finish:
  if (this.node().__transition) {
    // if there is transition data in any slot in the transition array, call the timer callback:
    var slots = this.node().__transition;
    var keys = Object.keys(slots);
    keys.forEach(function (d, i) {
      if (slots[d]) slots[d].timer._call();
    })
  }
  return this;
}

function projectPoint(x, y) {
  const point = mymap.latLngToLayerPoint(new L.LatLng(y, x));
  this.stream.point(point.x, point.y);
}

function projectMap([x, y]) {
  const point = mymap.latLngToLayerPoint(new L.LatLng(y, x));
  return [point.x, point.y];
}

var transform = d3.geoTransform({ point: projectPoint });

var path = d3.geoPath()
  .pointRadius(2)
  .projection(transform);

//ref: https://stackoverflow.com/a/56568406
var curve = function (context) {
  var custom = d3.curveLinear(context);
  custom._context = context;
  custom.point = function (x, y) {
    x = +x, y = +y;
    switch (this._point) {
      case 0: this._point = 1;
        this._line ? this._context.lineTo(x, y) : this._context.moveTo(x, y);
        this.x0 = x; this.y0 = y;
        break;
      case 1: this._point = 2;
      default:
        var x1 = this.x0 * 0.5 + x * 0.5;
        var y1 = this.y0 * 0.5 + y * 0.5;
        var m = 1 / (y1 - y) / (x1 - x);
        var r = -100; // offset of mid point.
        var k = r / Math.sqrt(1 + (m * m));
        if (m == Infinity) {
          y1 += r / 2;
          this._context.bezierCurveTo(x1 + r / 2, y1, x1 - r / 2, y1, x, y);
        }
        else {
          y1 += k;
          x1 += m * k;
          this._context.quadraticCurveTo(x1, y1, x, y);
        }
        this.x0 = x; this.y0 = y;
        break;
    }
  }
  return custom;
}

var line = d3.line()
  .x(function (d) {
    return projectMap([d[0], d[1]])[0];
  })
  .y(function (d) {
    return projectMap([d[0], d[1]])[1];
  })
  .curve(curve);

var svg = d3.select(mymap.getPanes().overlayPane)
  .append("svg");

var g = svg.append("g").attr("class", "leaflet-zoom-hide");

if (useDummy) {
  var capitalCities = [];
} else {
  var capitalCities = {};
}

function trailDelta() {
  return function (t) {
    let l = this.getTotalLength(),
      i = d3.interpolateString("0," + l, l + "," + l);
    return i(t);
  };
}

function transition(route, trail, color) {
  let l = route.node().getTotalLength();
  let duration = l * 5;

  trail.transition()
    .duration(duration)
    .attrTween("stroke-dasharray", trailDelta)
    .on("end", destinationPulse(route, duration, color))
    .remove();
}

function destinationPulse(route, delayDuration, color) {
  const dataCircle = route.datum();
  const pointDestination = projectMap(dataCircle[1]);

  function tStart(transition) {
    return transition
      .delay(delayDuration)
      .duration(100);
  }

  function tEnd(transition) {
    return transition
      .duration(500);
  }

  g.append("circle")
    .attr("cx", pointDestination[0])
    .attr("cy", pointDestination[1])
    .attr("r", 2)
    .attr("coord", dataCircle[1].toString())
    .style("fill", color)
    .style("fill-opacity", "1")
    .transition()
    .call(tStart)
    .attr("r", 3)
    .on("end", function (d) {
      d3.select(this)
        .transition()
        .call(tEnd)
        .attr("r", 10) // circle radius end
        .style("fill-opacity", "0")
        .on("end", function () {
          this.remove();
        });
    });

  mymap.on("zoom", reset);
  reset();

  function reset() {
    svg.selectAll("circle").select(function () {
      const el = d3.select(this);
      const coord = el.attr("coord").split(",");
      return el.attr("cx", projectMap(coord)[0])
        .attr("cy", projectMap(coord)[1]);
    });
  }

  route.remove();
}

var defaultCountry = "ID";

function fly(origin, destination, color) {
  if (!capitalCities[origin])
    origin = defaultCountry;
  if (!capitalCities[destination])
    destination = defaultCountry;

  //ref: https://stackoverflow.com/a/56568406
  let route = g.append("path")
    .datum([capitalCities[origin].coordinates, capitalCities[destination].coordinates])
    .attr("class", "route")
    .attr("d", line);

  let trail = g.append("path")
    .datum([capitalCities[origin].coordinates, capitalCities[destination].coordinates])
    .attr("class", "trail")
    .attr("d", line)
    .style("stroke-width", 1.5)
    .style("stroke", color);

  mymap.on("zoom", reset);
  reset();

  function reset() {
    trail.attr("d", line);
    route.attr("d", line);
  }

  transition(route, trail, color);
}

let colorAttack = {
  "Consecutive TCP small segments exceeding threshold": "Turquoise",
  "Reset outside window": "Blue",
  "(spp_ssh) Protocol mismatch": "Green",
  "(http_inspect) LONG HEADER": "Lime",
  "(http_inspect) UNESCAPED SPACE IN HTTP URI": "DarkRed",
  "Bad segment, adjusted size <= 0": "Aqua",
  "(http_inspect) TOO MANY PIPELINED REQUESTS": "Gold",
  "(spp_sdf) SDF Combination Alert": "PaleGreen",
  "(http_inspect) INVALID CONTENT-LENGTH OR CHUNK SIZE": "RoyalBlue",
  "(http_inspect) NO CONTENT-LENGTH OR TRANSFER-ENCODING IN HTTP RESPONSE": "Tomato"
};

function flyRandom() {
  const lenCapitalCities = 241;
  let rand1 = Math.floor(Math.random() * Math.floor(lenCapitalCities));
  let rand2 = Math.floor(Math.random() * Math.floor(lenCapitalCities));
  let rand1test = rand1 == 226 || rand1 == 227;
  let rand2test = rand2 == 226 || rand2 == 227;
  if (rand1 != rand2 && !rand1test && !rand2test) {
    let colorVal = "hsl(" + 360 * Math.random() + ',' +
      (25 + 70 * Math.random()) + '%,' +
      (30 + 10 * Math.random()) + '%)';
    fly(rand1, rand2, colorVal);
  }
}

if (useDummy) {
  setInterval(flyRandom, 100);
}

function flyX(d) {
  const arrKey = d.key.split("|");
  [d.src_country, d.coordinate, d.src_ip, d.alert, d.dest_name] = arrKey; // use destructuring assignment
  // console.log(d);
  switch (d.dest_name) {
    case 'vultr.eu':
      d.dest_country = "NL";
      break;
    case 'vultr.us':
      d.dest_country = "US";
      break;
    default:
      d.dest_country = defaultCountry;
      break;
  }
  if (d.src_country == "null")
    d.src_country = d.dest_country;

  let color = colorAttack[d.alert];
  if (!color) {
    //generate random color https://stackoverflow.com/a/43195379
    let colorVal = "hsl(" + 360 * Math.random() + ',' +
      (25 + 70 * Math.random()) + '%,' +
      (30 + 20 * Math.random()) + '%)';
    colorAttack[d.alert] = colorVal;
    color = colorVal;
  }

  fly(d.src_country, d.dest_country, color);
  updateTable(d, color);
}

function loaded(countries, capitals) {
  const feature = g.selectAll("path")
    .data(topojson.feature(capitals, capitals.objects.capitals).features)
    .enter()
    .append("path")
    .attr("class", "capitals")
    .attr("id", function (d) { return d.id; })
    .attr("d", path);

  mymap.on("zoom", reset);
  reset();

  // Reposition the SVG to cover the features.
  function reset() {
    const bounds = path.bounds(topojson.feature(countries, countries.objects.countries)),
      topLeft = bounds[0],
      bottomRight = bounds[1];
    // console.log(bounds.toString());

    svg.attr("width", bottomRight[0] - topLeft[0])
      .attr("height", bottomRight[1] - topLeft[1])
      .style("left", topLeft[0] + "px")
      .style("top", topLeft[1] + "px");

    g.attr("transform", "translate(" + -topLeft[0] + "," + -topLeft[1] + ")");
    feature.attr("d", path);

  }

  let geos = topojson.feature(capitals, capitals.objects.capitals).features;
  for (i in geos) {
    if (useDummy) {
      capitalCities[i] = {
        coordinates: geos[i].geometry.coordinates,
        name: geos[i].properties.country
      };
    } else {
      capitalCities[geos[i].id] = {
        coordinates: geos[i].geometry.coordinates,
        name: geos[i].properties.country
      };
    }
  }
}

let tableRows = 0, maxRow = 5;
function updateTable(data, color) {
  //insert new row
  $("<tr>" +
    `<td style='color:${color};'><span class="flag:${data.src_country}"></span> ${capitalCities[data.src_country].name}(${data.src_ip})</td>` +
    `<td style='color:${color};'><span class="flag:${data.dest_country}"></span> ${capitalCities[data.dest_country].name}(${data.dest_name})</td>` +
    `<td style='color:${color};'>${data.alert}</td>` +
    "<td>" + data.doc_count + "</td>" +
    "</tr>")
    .hide().appendTo("#stats").show();

  tableRows++;
  if (tableRows > maxRow || tableRows == 1)
    //remove first row
    $("#stats tr:first").remove();
}

$(function () {
  //ref: https://stackoverflow.com/a/49534634
  var files = ["/json/countries2.topo.json", "/json/capitals.topo.json"];
  var promises = [];
  files.forEach(function (url) {
    promises.push(d3.json(url))
  });

  Promise.all(promises)
    .then(function (values) {
      loaded(values[0], values[1])
    })
    .catch(error => {
      console.error(error.message)
    });
});
